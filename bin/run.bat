@echo off
TITLE Starter stregkodesystem...
cd ..
git pull
call composer install --optimize-autoloader
php bin\console cache:clear --env=prod
start "Stregkodesystem" cmd /k php bin\console server:run --env=prod
start "" http://localhost:8000