<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use GuzzleHttp\Client;
use Dandomain\Api\Api;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="home")
     */
    public function indexAction(Request $request)
    {
        $payApiUsername             = $this->getParameter('payapi_username');
        $payApiPassword             = $this->getParameter('payapi_password');
        $orderStateIdCaptured       = (int)$this->getParameter('order_state_id_captured');
        $orderStateIdNotCaptured    = (int)$this->getParameter('order_state_id_not_captured');
        $orderId                    = (int)$request->get('orderId', 0);
        $result                     = [];
        $dandomainApi               = new Api('https://www.2trendy.dk', $this->getParameter('dandomain_api_key'), new Client());

        try {
            if ($orderId) {
                $order = $dandomainApi->order->getOrder($orderId);
                if ($order->getStatusCode() != 200) {
                    throw new \InvalidArgumentException('Ordren findes ikke.');
                }
                $order = \GuzzleHttp\json_decode($order->getBody()->getContents());
                $comment = '';

                $client = new Client([
                    'timeout' => 10,
                    'connect_timeout' => 5,
                    'verify' => false,
                ]);

                $orderStateId = 0;
                $comment = 'Sendt';

                if ($order->transactionNumber && stripos($order->paymentInfo->name, 'viabill') === false) {
                    $response = $client->get('https://pay.dandomain.dk/PayApi.asp?username=' . $payApiUsername . '&password=' . $payApiPassword . '&capture=1&ShowStatusCodes=1&orderid=' . $orderId);

                    $statusCode = 404;
                    preg_match('/^[0-9]+/', $response->getBody()->getContents(), $matches);
                    if (!empty($matches)) {
                        $statusCode = (int)array_pop($matches);
                    }

                    $amount = 0;
                    preg_match('/\([0-9]+,[0-9]{2}\)$/', $response->getBody()->getContents(), $matches);
                    if (!empty($matches)) {
                        $amount = array_pop($matches);
                    }

                    $orderStateId = $orderStateIdCaptured;

                    switch ($statusCode) {
                        case 200:
                            $result['error'] = false;
                            $result['message'] = 'Ordrenummer ' . $orderId . ' blev hævet.';
                            $result['amount'] = $amount;
                            $comment = 'Hævet og sendt';
                            //$dandomainApi->order->setOrderState($orderId, $orderStateIdCaptured);
                            //$this->webhook($orderId, $orderStateIdCaptured);
                            break;
                        case 402:
                            $result['error'] = true;
                            $result['message'] = 'Ordrenummer ' . $orderId . ' kunne ikke hæves.';
                            //$dandomainApi->order->setOrderState($orderId, $orderStateIdNotCaptured);
                            //$this->webhook($orderId, $orderStateIdNotCaptured);
                            $orderStateId = $orderStateIdNotCaptured;
                            break;
                        case 403:
                            $result['error'] = true;
                            $result['message'] = $response->getBody()->getContents();
                            break;
                        case 404:
                            $result['error'] = true;
                            $result['message'] = 'Ordrenummer ' . $orderId . ' blev ikke fundet eller er allerede hævet.';
                            break;
                        case 405:
                            $result['error'] = true;
                            $result['message'] = $response->getBody()->getContents();
                            break;
                        case 406:
                            $result['error'] = true;
                            $result['message'] = $response->getBody()->getContents();
                            break;
                        case 407:
                            $result['error'] = true;
                            $result['message'] = $response->getBody()->getContents();
                            break;
                        case 409:
                            $result['error'] = true;
                            $result['message'] = $response->getBody()->getContents();
                            break;
                    }
                } else {
                    $result['error']    = false;
                    $result['message']  = 'Ordren er blevet hævet af Martin.';
                    $comment = 'Hævet og sendt';
                    $orderStateId = $orderStateIdCaptured;
                    //$dandomainApi->order->setOrderState($orderId, $orderStateIdCaptured);
                    //$this->webhook($orderId, $orderStateIdCaptured);
                }

                if($orderStateId) {
                    $dandomainApi->order->setOrderState($orderId, $orderStateId);
                    $this->webhook($orderId, $orderStateId);
                }

                if($comment) {
                    $dandomainApi->order->setOrderComment($orderId,$comment."\r\n\r\n".$order->comment);
                }
            }
        } catch(\Exception $e) {
            $result['error'] = true;
            $result['message'] = $e->getMessage();
        }
        // replace this example code with whatever you need
        return $this->render('AppBundle:Default:index.html.twig', [
            'result' => $result
        ]);
    }

    private function webhook($orderId, $orderState) {
        try {
            $baseUrls = ['http://admin.2trendy.dk', 'http://ehandel9000.dk'];

            $client = new Client([
                'timeout' => 10,
                'connect_timeout' => 5,
                'verify' => false,
            ]);

            foreach ($baseUrls as $baseUrl) {
                $client->patch($baseUrl.'/api/order/' . $orderId . '/order-state/' . $orderState);
            }
        } catch (\Exception $e) {

        }
    }
}
